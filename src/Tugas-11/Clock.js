import React, {Component} from 'react'

class Clock extends Component{
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleTimeString(),
      seconds: 100
    };
  }
  componentDidMount() {
    this.intervalID = setInterval(
      () => this.tick(),
      1000
    );
    this.myInterval = setInterval(() => {
            const {seconds} = this.state

            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            } else {
                    clearInterval(this.myInterval)
                }
            
        }, 1000)
  }
  componentWillUnmount() {
    clearInterval(this.intervalID);
    clearInterval(this.myInterval);
  }
  tick() {
    this.setState({
      time: new Date().toLocaleTimeString()
    });
  }
  render() {
      const { seconds } = this.state
        return (
            <div>
                { seconds === 0
                    ? <h1></h1>
                    : <h1>sekarang jam : {this.state.time}<br />hitung mundur : {seconds}</h1>
                    
                }
            </div>
      
    );
  }
}

export default Clock;

