import React from 'react';
import {
    Switch,
    Route,
    Link
} from "react-router-dom";

import Table from '../Tugas-12/Table'
import FoodTableHook from '../Tugas-13/FoodTableHook'
import FoodTableApp from './FoodTableApp'

const Routes = () =>{
    return(
       <>
        <nav>
            <ul>
                <li>
                    <Link to="/Tugas-13">tugas13</Link>
                </li>
                <li>
                    <Link to="/Tugas-14">tugas14</Link>
                </li>
            </ul>
        </nav>
        <Switch>
            <Route path='/Tugas-12'>
                <Table />
            </Route>
            <Route path='/Tugas-13'>
                <FoodTableHook />
            </Route>
            <Route path = '/' component={FoodTableApp}/>
        </Switch>
        </>
    )

}
export default Routes