import React, { Component } from 'react';
import './Table.css';

class Table extends React.Component {
  render() {
    const items = this.props.items;
    return (
      <div id="Table">
        <table id="items">
          <tbody>

            {items.map(item => {
              return (
                <tr>
                  <td>{item.nama}</td>
                  <td>{item.harga}</td>
                  <td>{item.berat/1000 + "kg"}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;