import React, { Component } from 'react'
import './Table.css';

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class Table extends Component {
   constructor(props) {
      super(props) //since we are extending class Table so we have to use super in order to override Component class constructor
      this.state = { //state is by default an object
         isi_tabel: dataHargaBuah
      }
   }

   renderTableData() {
      return this.state.isi_tabel.map((buah, index) => {
         const { nama, harga, berat } = buah //destructuring
         return (
            <tr>
               <td>{nama}</td>
               <td>{harga}</td>
               <td>{berat/1000 + "kg"}</td>
            </tr>
         )
      })
   }

   render() {
      return (
         <div>
            <h1 id='title'>Daftar Harga Buah</h1>
            <table id='isi_tabel'>
               <tbody>
                  {this.renderTableData()}
               </tbody>
            </table>
         </div>
      )
   }

renderTableHeader() {
      let header = Object.keys(this.state.isi_tabel[0])
      return header.map((key, index) => {
         return <th key={index}>{key[0].toUpperCase()+key.substr(1)}</th>
      })
   }

   render() {
      return (
         <div>
            <h1 id='title'>Tabel Harga Buah</h1>
            <table id='isi_tabel'>
               <tbody>
                  <tr>{this.renderTableHeader()}</tr>
                  {this.renderTableData()}
               </tbody>
            </table>
         </div>
      )
   }
}

export default Table

