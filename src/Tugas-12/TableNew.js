import React, { Component } from 'react';
import Table from './Table';
import Form from './Formulir';

class TableNew extends Component {
  constructor() {
    super();

    this.state = {
      nama: '',
      harga: '',
      berat: '',
      items: []
    }
  };

  handleFormSubmit = (e) => {
    e.preventDefault();

    let items = [...this.state.items];

    items.push({
      nama: this.state.nama,
      harga: this.state.harga,
      berat: this.state.berat
    });

    this.setState({
      items,
      username: '',
      password: '',
      berat: ''
    });
  };

  handleInputChange = (e) => {
    let input = e.target;
    let name = e.target.name;
    let value = input.value;

    this.setState({
      [name]: value
    })
  };

  render() {
    return (
      <div className="Table">
        <Table items={ this.state.items }/>
        <Form handleFormSubmit={ this.handleFormSubmit } 
          handleInputChange={ this.handleInputChange }
          newNama={ this.state.username }
          newHarga={ this.state.password }
          newBerat={ this.state.berat } />
      </div>
    );
  }
}


export default TableNew;
