import React, { Component } from 'react';

class Form extends React.Component {
  render() {
    return (
      <div id="Form">
        <h3>Add a new item to the table:</h3>  
        <form onSubmit={this.props.handleFormSubmit}>
          <label for="nama">
          Nama:
          <input id="nama" value={this.props.newNama} 
            type="text" name="nama"
            onChange={this.props.handleInputChange} />
          </label>
          <label for="harga">
          Harga:
          <input id="harga" value={this.props.newHarga} 
            type="number" name="password"
            onChange={this.props.handleInputChange} />
          </label>
          <label for="berat">
          Berat:
          <input id="berat" value={this.props.newBerat} 
            type="number" name="berat"
            onChange={this.props.handleInputChange} />
          </label>
          <button type="submit" value="Submit">Add Item</button>
        </form>
      </div>
    );
  }
}

export default Form;